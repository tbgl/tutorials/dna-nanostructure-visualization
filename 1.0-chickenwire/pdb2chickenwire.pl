#!/usr/bin/perl -w

use strict;
use warnings;
use Getopt::Long;

###################################
my $orig_pattern = "(ATOM  )(.{5}).(.{4})(.)(.{3}).(.)(.{4})(.)...(.{8})(.{8})(.{8})(.{6})(.{6})";
my $chm_pattern = "(ATOM  )(.{5}).(.{4})(.)(.{4})(.)(.{4})(.)...(.{8})(.{8})(.{8})(.{6})(.{6})";

my $pattern = $chm_pattern;
#$pattern = $orig_pattern if defined $original;
###################################

my $ndxfile;
my $format = "%10.5f";
my $help;

GetOptions(
    "ndx=s" => \$ndxfile,
    "help" => \$help
);

sub usage {
    print STDERR "Usage: pdb2chickenwire.pl --ndx=chickenwire.for.make_ndx allatom.PDB > chickenwire.pdb\n";
    exit;
}

&usage unless defined $ndxfile;
&usage if defined $help;

###################################
# Read chickenwire.for.make_ndx from cadnano2pdb
my @basepairs;
my @res2bp;
my @basepairsH;
my @basepairsB;
my @basepairsL;
print STDERR "Reading index file $ndxfile\n";
open FH, $ndxfile or die "Error: Can't open $ndxfile\n";
while (<FH>) {
    chomp;
    if (/^ri/) {
	my ($tt, $ri, $rj) = split /\s+/;
	push @basepairs, [$ri-1,$rj-1]; # we're using zero-based indexing.
	$res2bp[$ri-1] = $#basepairs;
	$res2bp[$rj-1] = $#basepairs;
    }
    elsif (/^name\s+[0-9]+\s+(H[0-9]+)(B[0-9]+)(L[0-9]+)$/) {
	#print STDERR "H=$1 L=$2 L=$3\n";
	push @basepairsH, $1;
	push @basepairsB, $2;
	push @basepairsL, $3;
    }
}
close FH;
printf STDERR "There are %d base pairs defined in index file.\n", $#basepairs+1;

###################################
# Read PDB
print STDERR "Reading PDB file from STDIN\n";
my @xyz; # chickenwire points.
my @xyzN; # atoms per base pair must be always 15. Explicit counting just in case.
my $ri = -1;	# residue counter
my $oldresid = -99999;
my $model = 0;
while (<>) {
    if (/^(TER|END)/) {
	&print_reset;
	next;
    }

    next unless (/^(ATOM  )(.{5}).(.{4})(.)(.{4})(.)(.{4}).{4}(.{8})(.{8})(.{8})(.*)/);
    
    my ($name,$resname,$resid,$x,$y,$z) = ($3,$5,$7,$8,$9,$10);
    # we count only base heavy atoms
    next unless $name =~ s/^\s+(N1|C2|N3|C4|C5|C6|N7|C8|N9)\s+$/$1/;
    #print STDERR "$name,$resname,$resid,$x,$y,$z\n";
    
    if ($resid != $oldresid) {
	# new residue.
	++$ri;	# increase residue counter.
	$oldresid = $resid;
    }
    my $bp = $res2bp[$ri];

    #printf STDERR "residue mapping: $ri => $bp\n";
    @{$xyz[$bp]} = (0,0,0) unless defined $xyz[$bp];
    @{$xyz[$bp]} = vec_add(@{$xyz[$bp]}, ($x,$y,$z)); 
    ++$xyzN[$bp];
}

&print_reset if @xyz;

exit;


sub print_reset {
    # print chickenwire PDB.
    printf "MODEL%9d\n", ++$model;
    for my $bp (0 .. $#xyz) {
	printf "%-6s%5d %-4s%1s%3s %1s%4d%1s   %8.3f%8.3f%8.3f\n",
	    "ATOM", $bp+1, $basepairsB[$bp], " ", $basepairsH[$bp], "", $bp+1, "", vec_scale(@{$xyz[$bp]},1/$xyzN[$bp]);
    }
    print "TER\nENDMDL\n";

    # reset for the next frame
    $ri = -1;
    $oldresid = -99999;
    undef @xyz;
    undef @xyzN;
}

sub vec_inner {
    return ($_[0]*$_[3]+$_[1]*$_[4]+$_[2]*$_[5]);
}

sub vec_cross {
    return ($_[1]*$_[5]-$_[2]*$_[4],
            $_[2]*$_[3]-$_[0]*$_[5],
            $_[0]*$_[4]-$_[1]*$_[3]);
}

sub vec_sub {
    return ($_[0]-$_[3],$_[1]-$_[4],$_[2]-$_[5]);
}

sub vec_add {
    return ($_[0]+$_[3],$_[1]+$_[4],$_[2]+$_[5]);
}

sub vec_norm {
    return sqrt($_[0]*$_[0]+$_[1]*$_[1]+$_[2]*$_[2]);
}

sub vec_unit {
    my $len = vec_norm(@_);

    return ($_[0]/$len,$_[1]/$len,$_[2]/$len);
}

sub vec_scale {
    return ($_[0]*$_[3],$_[1]*$_[3],$_[2]*$_[3]);
}
