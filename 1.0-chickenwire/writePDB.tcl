# load trajectory, write a multiframe PDB 
mol new hextube.psf
animate read dcd equil_k0.dcd beg 0 end -1 waitfor all
set n [ molinfo top get numframes ]
puts "  Loaded $n frames."
animate write pdb {equil_k0.pdb} beg 0 end -1 sel [atomselect top all]

quit
