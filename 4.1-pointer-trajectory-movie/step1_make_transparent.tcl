#A smooth transition from a cylindrical to a quicksurf representation
#over the course of num_frames
set num_frames_step1 50
#set display size
display resize 800 600
scale by 1.2
material add copy AOEdgy

#kill the lights
light 0 off
light 1 off
light 2 off
light 3 off

#Render first frame
render TachyonLOptiXInternal frames/frame_0.tga
#create quicksurf
mol color ColorID 0
mol representation QuickSurf 5.7 1.0 4.0 1.0
mol selection all
mol material Material29
mol addrep 0
#quicksurf starts transparent
material change opacity Material29  0.0

#set graphics 
graphics 0 materials on
material add copy AOChalky
graphics 0 material Material30


for {set i 1 } {$i <= $num_frames_step1} {incr i } {
    set m [expr  (1.0/$num_frames_step1) * $i]
    material change opacity Material29  $m #quicksurf appear
    material change opacity Material30  [expr 1.0 - $m] #cylinders disapper
    #delete cylinders at low transparency for smoother transition
    if { [expr 1.0 - $m] < 0.15} {
        graphics 0 delete all
    }
    render TachyonLOptiXInternal frames/frame_${i}.tga
}

