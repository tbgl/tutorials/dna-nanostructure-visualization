#continuing from step1_make_transparent.tcl

#add representations for this scene

material add copy AOEdgy
material rename Material31 cpkframe

mol representation CPK 8.000000 7.000000 50.000000 50.000000
mol color ColorID 8
mol selection {occupancy 40 to 49}
mol material cpkframe
mol addrep top


material add copy AOEdgy
material rename Material32 qsfront

mol representation QuickSurf 6.500000 0.500000 2.700000 1.000000
mol color ColorID 8
mol selection {not index 1825 1826 and (occupancy <40)}
mol material qsfront
mol addrep top


material add copy AOEdgy
material rename Material33 qsback

mol representation QuickSurf 6.500000 0.500000 2.700000 1.000000
mol color ColorID 0
mol selection {not index 1825 1826 and (occupancy >49)}
mol material qsback
mol addrep top

#make materials transparent
material change opacity cpkframe 0.0
material change opacity qsfront  0.0
material change opacity qsback   0.0

#50 frame transition
set num_frames_step2 50

for {set i 0 } {$i <= $num_frames_step2} {incr i } {
    set actualFrame [expr $num_frames_step1 + $i]
    set m [expr  (1.0/$num_frames_step2) * $i]
    #qsffront surface should be partially visible
    set m_qsfront [expr  (0.26/$num_frames_step2) * $i]
    #reps to appear
    material change opacity cpkframe $m
    material change opacity qsfront  $m_qsfront
    material change opacity qsback   $m
    #rep to disapper
    material change opacity Material29  [expr 1.0 - $m]
    render TachyonLOptiXInternal frames/frame_${actualFrame}.tga
}

