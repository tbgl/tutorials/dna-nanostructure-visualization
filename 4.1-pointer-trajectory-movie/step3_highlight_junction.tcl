#continuing from step2_showPlane
#phase out qsfront and highlight junction
#make movie of dcd

material add copy AOEdgy
material rename Material34 junctionhl

mol representation CPK 13.000000 10.000000 50.000000 50.000000
mol color ColorID 3
mol selection {beta 125 to 130 and (occupancy 40 to 43)}
mol material junctionhl
mol addrep top

set num_frames_step3 25
for {set i 0 } {$i <= $num_frames_step3 } {incr i } {
    set actualFrame [expr $num_frames_step1 + $num_frames_step2 + $i]
    set m [expr  (1.0/$num_frames_step3) * $i]
    #qsffront surface should be partially visible
    set m_qsfront [expr  0.26 - (0.26/$num_frames_step3) * $i]
    #reps to disappear
    material change opacity qsfront $m_qsfront
    #rep to appear
    material change opacity junctionhl $m
    render TachyonLOptiXInternal frames/frame_${actualFrame}.tga
}

#remove useless reps
mol delrep 0 0
mol delrep 0 0
mol delrep 0 0
mol delrep 0 0
mol delrep 0 0
mol delrep 0 0
mol delrep 1 0
#smooth the reps for the movie
mol smoothrep top 0 5
mol smoothrep top 1 5
mol smoothrep top 2 5
#
#render the rest of the movie
set nf [molinfo top get numframes ]
#render every 3 frames
animate delete beg 0 end $nf skip 3
for {set i 0} {$i < [expr $nf/3] } {incr i} {
    set actualFrame [expr $num_frames_step1 + $num_frames_step2 + $num_frames_step3 +  $i]
    animate goto $i
    render TachyonLOptiXInternal frames/frame_${actualFrame}.tga
}

