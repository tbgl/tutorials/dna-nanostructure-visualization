import numpy as np
from moviepy.editor import *

#1
startFrame, endFrame = 0, 50
frames = list(map(lambda x: 'frames/frame_{}.png'.format(int(x)),np.arange(startFrame,endFrame)))
clip1 = ImageSequenceClip(frames,fps=25).resize(0.55).speedx(0.5)
cX, cY = clip1.size
clip0 = ImageClip(f'frames/frame_0.png',duration=4)\
    .resize((cX,cY))
num1 = ImageClip(f'images/one.png',duration=4)\
        .resize(0.1)\
        .set_pos((15,15))\
        .set_start(0)
rep1 = ImageClip(f'images/rep1.png',duration=4)\
        .resize(0.1)\
        .set_pos('bottom')\
        .set_start(0)

#2
startFrame, endFrame = 50, 100
frames = list(map(lambda x: 'frames/frame_{}.png'.format(int(x)),np.arange(startFrame,endFrame)))
clip3 = ImageClip(f'frames/frame_{startFrame}.png',duration=4)\
    .resize((cX,cY))
clip4 = ImageSequenceClip(frames,fps=25).resize(0.55).speedx(0.5)
num2 = ImageClip(f'images/two.png',duration=4)\
        .resize(0.1)\
        .set_pos((15,15))
rep2 = ImageClip(f'images/rep2.png',duration=4)\
        .resize(0.1)\
        .set_pos('bottom')\

#3
startFrame, endFrame = 100 , 125
frames = list(map(lambda x: 'frames/frame_{}.png'.format(int(x)),np.arange(startFrame,endFrame)))
clip5 = ImageClip(f'frames/frame_{startFrame}.png',duration=4)\
    .resize((cX,cY))
clip6 = ImageSequenceClip(frames,fps=25).resize(0.55).speedx(0.5)
num3 = ImageClip(f'images/three.png',duration=4)\
        .resize(0.1)\
        .set_pos((15,15))
rep3 = ImageClip(f'images/rep3.png',duration=4)\
        .resize(0.1)\
        .set_pos('bottom')

#4 Highlight the 3 stack junction
clip7 = ImageClip(f'frames/frame_124.png',duration=2)\
    .resize((cX,cY))
num4 = ImageClip(f'images/four.png',duration=6)\
        .resize(0.1)\
        .set_pos((15,15))
#skipping rep4.png
rep4 = ImageClip(f'images/rep5.png',duration=6)\
        .resize(0.1)\
        .set_pos('bottom')
#blur in
clip8 = ImageClip(f'images/frame_124a.png',duration=2.4)\
    .resize((cX,cY))

#5 movie
startFrame, endFrame = 125 , 458
frames = list(map(lambda x: 'frames/frame_{}.png'.format(int(x)),np.arange(startFrame,endFrame)))
clip9 = ImageSequenceClip(frames,fps=25).resize(0.55).speedx(0.75)
num5 = ImageClip(f'images/five.png',duration=17)\
        .resize(0.1)\
        .set_pos((15,15))
#skipping rep4.png
rep5 = ImageClip(f'images/rep6.png',duration=17)\
        .resize(0.1)\
        .set_pos('bottom')

#Joining the clips
Final_clip = CompositeVideoClip([
	clip0.set_start(0),
    clip1.set_start(4),
    num1.set_start(0).crossfadeout(0.2).crossfadein(0.2),
    rep1.set_start(0).crossfadeout(0.2).crossfadein(0.2),
	clip3.set_start(8),
    clip4.set_start(12),
    num2.set_start(8).crossfadeout(0.2).crossfadein(0.2),
    rep2.set_start(8).crossfadeout(0.2).crossfadein(0.2),
    clip5.set_start(16),
    clip6.set_start(18),
    num3.set_start(16).crossfadeout(0.2).crossfadein(0.2),
    rep3.set_start(16).crossfadeout(0.2).crossfadein(0.2),
    clip7.set_start(20),
    clip8.set_start(22),
    clip7.set_start(24),
    num4.set_start(20).crossfadeout(0.2).crossfadein(0.2),
    rep4.set_start(20).crossfadeout(0.2).crossfadein(0.2),
    clip9.set_start(26),
    num5.set_start(26).crossfadein(0.2),
    rep5.set_start(26).crossfadein(0.2)
    ],
   size=(cX,cY))

#rendering the moviE
Final_clip.write_videofile(f'Final_clip.mp4',\
		codec='mpeg4',threads=14,\
        preset='veryslow',ffmpeg_params=['-tune','film','-crf', '0','-b:v','6000k'])

