proc draw_stage {z} {
    graphics top material AOChalky
    # graphics top color white                                                 
    graphics top color silver
    color change rgb silver 0.65
    graphics top triangle "-9000 -9000 $z" "9000 -9000 $z" "9000 9000 $z"
    graphics top triangle "-9000 -9000 $z" "-9000 9000 $z" "9000 9000 $z"

    display aasamples 64
    # display aosamples 128                                                    
    # display aosamples 512                                                    
    render aasamples TachyonLOptiXInternal 8
    render aosamples TachyonLOptiXInternal 256
    #display aoambient 2.0
    #display aodirect 0
}
