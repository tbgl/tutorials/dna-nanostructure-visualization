set PSF stickman-1.psf
set DCD stickman-1.0.dcd
set ID [mol new $PSF]
mol addfile $DCD waitfor all

color Display Background white
display backgroundgradient off
display depthcue 0
display projection Perspective
display distance -1
display height 1

color change rgb blue 0.27 0.52 0.9
color change rgb red 0.9 0.45 0.33

mol material AOChalky
mol representation QuickSurf 5.700000 1.000000 4.000000 1.000000
mol selection "all"
mol color colorID 0
mol addrep $ID

display aodirect 0.600000
light 0 off
display shadows on
display ambientocclusion on
display aoambient 0.900000
display dof off
display dof_fnumber 660.000000
display dof_focaldist 3.700000

render aasamples TachyonInternal 16
render aosamples TachyonInternal 64

display resize 800 800
display update ui
rotate x by -90
display update ui

scale by 0.4
render TachyonInternal stickman-tach-inter.ppm
#render TachyonLOptiXInternal stickman-tach-optix.ppm

#frameLoop f {
#    animate goto $f
#    render TachyonLOptiXInternal [format frames/stickman.%03d.ppm $f] ""
#}

#exit
